import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: () => import('./core/authentication/authentication.module').then(m => m.AuthenticationModule) },
  { path: 'register', loadChildren: () => import('./core/authentication/register/register.module').then(m => m.RegisterModule) },
  { path: 'inicio', loadChildren: () => import('./core/home/home.module').then(m => m.HomeModule) },
  { path: '**', loadChildren: () => import('./shared/shared.module').then(m => m.SharedModule) }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
