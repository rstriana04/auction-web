import { ActionReducerMap } from '@ngrx/store';
import { authenticationReducer, AuthenticationState } from '../../core/authentication/store/reducers/authentication.reducer';

export interface AppState {
  authentication: AuthenticationState;
}

export const appReducer: ActionReducerMap<AppState> = {
  authentication: authenticationReducer
};
