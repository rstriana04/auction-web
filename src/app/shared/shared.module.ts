import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatAutocompleteModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';

import { NotFoundComponent } from './not-found/not-found.component';

import { SharedRoutingModule } from './shared-routing.module';

const ModulesMaterial = [
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatSlideToggleModule,
  LayoutModule,
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatDialogModule,
  MatTabsModule,
  MatCheckboxModule,
  MatAutocompleteModule,
  MatExpansionModule,
  MatDatepickerModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatStepperModule,
  MatChipsModule,
  MatDividerModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSnackBarModule,
  MatCardModule,
  MatTooltipModule,
  MatBottomSheetModule,
  MatMenuModule,
  MatSlideToggleModule,
  MatGridListModule,
  MatProgressBarModule
];

@NgModule({
  declarations: [NotFoundComponent],
  imports: [
    CommonModule,
    SharedRoutingModule,
    ReactiveFormsModule,
    ...ModulesMaterial
  ], exports: [...ModulesMaterial, ReactiveFormsModule]
})
export class SharedModule {}
