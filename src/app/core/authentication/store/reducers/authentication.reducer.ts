import { Action, createReducer } from '@ngrx/store';

export interface AuthenticationState {
  message: string;
  userAuthenticated: any;
}

const initialStateAuthentication: AuthenticationState = {
  message: '',
  userAuthenticated: false
};

const reducer = createReducer(initialStateAuthentication);

export function authenticationReducer(state: AuthenticationState | undefined, action: Action) {
  return reducer(state, action);
}
