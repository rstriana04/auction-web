import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LogInService } from './services/log-in.service';

@Component({
  selector: 'test-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public hide = true;
  public formLogin: FormGroup;

  constructor(private loginService: LogInService, private router: Router) {
    this.initFormRegister();
  }

  ngOnInit() {
  }

  private initFormRegister() {
    this.formLogin = new FormGroup({
      email: new FormControl('', Validators.compose([Validators.required])),
      password: new FormControl('', Validators.compose([Validators.required]))
    });
  }

  public onSendLogin(formLogin: FormGroup) {
    if ( formLogin.valid ) {
      this.loginService.signIn(formLogin.value).subscribe(e => {
        localStorage.setItem('currentUser', JSON.stringify(e.token));
        console.log(e.token);
        this.router.navigate(['/inicio']);
      });

      // this.loginService.signIn(formLogin.value).subscribe(e => {
      //   console.log(e);
      // });
    }
  }

  public redirect() {
    this.router.navigate(['/register']);
  }
}
