import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Login } from '../models/login';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LogInService {

  constructor(private httpClient: HttpClient) { }

  private httpOptions1 = {
    headers: new HttpHeaders(
    { 'Content-Type': 'application/json',
      'Authorization': 'Basic ZmFndXptYW5AZ21haWwuY29tOmZhYml0bw==',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
      'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With',
      'X-Random-Shit': '123123123'
    })
  };
  public signIn(login: Login): Observable<any> {
    console.log(login);
    const httpOptions = {
      headers: new HttpHeaders(
      { 'Content-Type': 'application/json',
        'Authorization': 'Basic ' + window.btoa(login.email + ':' + login.password),
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With',
        'X-Random-Shit': '123123123'
      })
    };
    return this.httpClient.post(`${environment.apiUrl}/auth/sign-in`, login, httpOptions

    );
  }
}
