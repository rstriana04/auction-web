import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  constructor(
    private httpClient: HttpClient
  ) { }

  public registerUser(user: User): Observable<any> {
    return this.httpClient.post(`${ environment.apiUrl }/auth/sign-up`, user , {
      headers: { 'Content-Type': 'application/json' },
      observe: 'response',
      responseType: 'json'
    });
  }

}
