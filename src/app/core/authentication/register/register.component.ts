import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RegisterService } from './services/register.service';

@Component({
  selector: 'test-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterComponent implements OnInit {
  public hide = true;
  public formRegister: FormGroup;

  constructor(
    private registerService: RegisterService,
    private toastService: ToastrService,
    private router: Router
  ) {
    this.initFormRegister();
  }

  ngOnInit() {

  }

  private initFormRegister() {
    this.formRegister = new FormGroup({
      name: new FormControl('', Validators.compose([Validators.required])),
      email: new FormControl('', Validators.compose([Validators.required])),
      password: new FormControl('', Validators.compose([Validators.required])),
      isAdmin: new FormControl(false)
    });
  }

  public onSendRegister(formRegister: FormGroup) {
    if ( formRegister.valid ) {
      this.registerService.registerUser(formRegister.value).subscribe(response => {
        console.log(response);
        this.toastService.success('¡Usuario registrado correctamente!', '¡Correcto!', {timeOut: 3000});
      }, error => {
        console.error({error});
        this.toastService.error('¡Ocurrio un error registrando el usuario!', '¡Error!', {timeOut: 3000});
      });
    }
  }

  public redirect() {
    this.router.navigate(['/login']);
  }
}
