import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupAddProductsComponent } from './popup-add-products.component';

describe('PopupAddProductsComponent', () => {
  let component: PopupAddProductsComponent;
  let fixture: ComponentFixture<PopupAddProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupAddProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupAddProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
