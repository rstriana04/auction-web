import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(
    private httpClient: HttpClient
  ) { }

  private httpOptions = {
    headers: new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1ZTYzZWZiNWU0MDcwNDNhNzJhNDIzNTkiLCJuYW1lIjoiZmFiaWFuIiwiZW1haWwiOiJmYWd1em1hbkBnbWFpbC5jb20iLCJpYXQiOjE1ODM2MTk1MDQsImV4cCI6MTU4MzYyMDQwNH0.WDR2CMzvdxxoF4mKlqGM-VI-epC8ThdwV5uG2qicI80',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With',
        'X-Random-Shit': '123123123'
      })
  };

  getProducts(): Observable<any> {
    return this.httpClient.get(`${ environment.apiUrl }/product`, this.httpOptions);
  }

}
