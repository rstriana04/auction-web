import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { ShowProductsComponent } from './show-products/show-products.component';
import { PopupAddProductsComponent } from './popup-add-products/popup-add-products.component';


@NgModule({
  declarations: [ProductsComponent, ShowProductsComponent, PopupAddProductsComponent],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    SharedModule
  ]
})
export class ProductsModule { }
