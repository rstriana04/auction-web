import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PopupAddProductsComponent } from '../popup-add-products/popup-add-products.component';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'test-show-products',
  templateUrl: './show-products.component.html',
  styleUrls: ['./show-products.component.scss']
})
export class ShowProductsComponent implements OnInit {
  public data$: Observable<any> = of([]);
  constructor(
    private matDialog: MatDialog,
    private productsService: ProductsService
  ) { }

  ngOnInit() {
    const token = localStorage.getItem('currentUser');
    console.log(token);
    this.data$ = this.productsService.getProducts().pipe(
      map(data => data.data)
    );
    this.data$.subscribe(e => {
      console.log(e);
    });

  }

  public showDialogCreateProduct() {
    this.matDialog.open(PopupAddProductsComponent, {
      height: '400px',
      width: '400px'
    });
  }
}
